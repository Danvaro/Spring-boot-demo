package boot.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor
public class PersonEntity {

    @Id
    @GeneratedValue
    Long id;

    @Column(name = "name")
    String name;
}
