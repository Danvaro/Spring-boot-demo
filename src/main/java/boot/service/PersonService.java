package boot.service;

import boot.persistence.entity.PersonEntity;
import boot.persistence.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public PersonEntity getPersonName(Long id) {
        return this.personRepository.findOne(id);
    }

    public List<PersonEntity> getAllPersons() {
        Iterable<PersonEntity> source = this.personRepository.findAll();
        List<PersonEntity> personEntities = new ArrayList<>();
        source.forEach(personEntities::add);

        return personEntities;
    }
}
