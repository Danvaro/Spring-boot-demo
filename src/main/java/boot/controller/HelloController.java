package boot.controller;

import boot.persistence.entity.PersonEntity;
import boot.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/persons")
public class HelloController {

    @Autowired
    PersonService personService;

    @GetMapping
    @RequestMapping("/{id}")
    public PersonEntity getPersonNameById(@PathVariable Long id) {
        return this.personService.getPersonName(id);
    }

    @GetMapping
    public List<PersonEntity> getAllPersons() {
        return this.personService.getAllPersons();
    }
}
